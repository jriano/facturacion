﻿using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CrearUsuariosDolibarr
{
    class Program
    {
        static void Main(string[] args)
        {
            CargarArchivo();
            Console.WriteLine("Hello World!");
        }
        static void CargarArchivo()
        {
            var filePath = @"C:/Facturacion/Facturacion.xlsx";
            FileInfo file = new FileInfo(filePath);
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                int rowCount = worksheet.Dimension.Rows;
                int ColCount = worksheet.Dimension.Columns;

                List<String> rawText = new List<string>();
                List<Factura> resultado = new List<Factura>();
                List<empresas> rawText1 = new List<empresas>();
                List<Factura> factura = new List<Factura>();
                //////USUARIOS
                for (int row = 1; row <= rowCount; row++)
                {
                    int col = 1;
                    rawText.Add(worksheet.Cells[row, col].Value.ToString());
                }
                //EMPRESAS
                for (int row = 1; row <= rowCount; row++)
                {
                    int col = 1;
                    empresas empresas = new empresas();
                    empresas.id = worksheet.Cells[row, col].Value.ToString();
                    empresas.nombre = worksheet.Cells[row, 2].Value.ToString();
                    rawText1.Add(empresas);
                }
                ////EMPRESAS Y USUARIOS
                foreach (var item in rawText)
                {
                    Console.WriteLine(ServiceRe(item));
                }
                ////FACTURACION
                for (int row = 1; row <= rowCount; row++)
                {
                    int col = 1;
                    Factura factura_ = new Factura();
                    factura_.id = 1;
                    factura_.user_id = worksheet.Cells[row, 2].Value.ToString();
                    factura_.description = "Factura de servicios prepagados, segunda quincena de Noviembre (Ciudad " + worksheet.Cells[row, col].Value.ToString() + ")";
                    factura_.value = Convert.ToDouble(worksheet.Cells[row, 3].Value.ToString());
                    factura_.quantity = 1;
                    factura.Add(factura_);
                }
                ////FACTURACION
                foreach (var item in factura)
                {
                    Factura fac_datos = new Factura();
                    fac_datos = ServiceCrearFactura(item);
                    Console.WriteLine("ITEM : " + item.user_id + "Numero Factura" + fac_datos.data.invoice_erp_ref);
                }
            }
        }

        static Factura ServiceCrearFactura(Factura name)
        {

            HttpClient httpclientCreateService = new HttpClient();
            httpclientCreateService.BaseAddress = new Uri("http://52.40.47.88");
            httpclientCreateService.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpclientCreateService.DefaultRequestHeaders.Add("access_token", "55457cc75a4321f952bfc3abeb16e20a7da55237");
            using (HttpResponseMessage responseCreateService = httpclientCreateService.PostAsync("/mu_phalcon_api/task/invoice/", new StringContent(JsonConvert.SerializeObject(name), Encoding.UTF8, "application/json")).Result)
            {
                using (HttpContent contentresult = responseCreateService.Content)
                {
                    Task<string> result = contentresult.ReadAsStringAsync();
                    Factura desere = JsonConvert.DeserializeObject<Factura>(result.Result);
                    return desere;
                }
            }
        }
        static string ServiceRe(string name)
        {
            Usuario usuario = new Usuario();
            usuario.type = 3;
            usuario.user_create = 1;
            usuario.province = "";
            usuario.company_name = "";
            HttpClient httpclientCreateService = new HttpClient();
            httpclientCreateService.BaseAddress = new Uri("https://cerberus.mensajerosurbanos.com");
            httpclientCreateService.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpclientCreateService.DefaultRequestHeaders.Add("access_token", "0e235231e8926a74d1e4d208de1297d7962703ca");
            using (HttpResponseMessage responseCreateService = httpclientCreateService.PutAsync("/erp/" + name, new StringContent(JsonConvert.SerializeObject(usuario), Encoding.UTF8, "application/json")).Result)
            {
                using (HttpContent contentresult = responseCreateService.Content)
                {
                    Task<string> result = contentresult.ReadAsStringAsync();
                    return result.Result;
                }
            }
        }
    }
}
