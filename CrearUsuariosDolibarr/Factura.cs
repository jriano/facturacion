﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrearUsuariosDolibarr
{
    public class Factura
    {
        public int id { get; set; }
        public string user_id { get; set; }
        public string description { get; set; }
        public double value { get; set; }
        public int quantity { get; set; }
        public ResponseFactura data { get; set; }
        public string factura { get; set; }
    }
}
